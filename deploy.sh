#!/bin/bash

rm klishe.db
rm companies.json
./scripts/create_db.pl
echo '1 - Database was created successfully';
./scripts/add_services.pl
echo '2 - Services was added successfully';
./scripts/curl.sh
echo '3 - JSON with companies data was downloaded successfully';
./scripts/insert_companies.pl
echo '4 - Companies was parsed and inserted in database successfully';
./scripts/add_test_emails.pl
echo '5 - Real emails in database was replaced with test emails successfully';
./scripts/add_random_services.pl
echo '6 - Random services was added successfully';
echo "DATABASE DEPLOYED";