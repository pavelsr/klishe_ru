#!/usr/bin/env perl

use common::sense;
use feature 'say';
# use File::Slurp;
use Mojo::JSON;
use Data::Dumper;

### version 1 - special function

sub slurp {
    my $file = shift;
    open my $fh, '<', $file or die;
    local $/ = undef;
    my $cont = <$fh>;
    close $fh;
    return $cont;
}

my $file = 'companies.json';
my $content = slurp($file);
# warn Dumper $content;

my $companies = Mojo::JSON::decode_json $content;  # decode_json , from_json or j
warn Dumper scalar @$companies;

### end of version 1



# use Path::Class;
# use autodie; # die if problem reading or writing a file
# my $dir = dir("/home/pavel/projects/klishe_ru"); 
# my $file = $dir->file("test2.json");

# # Read in the entire contents of a file
# my $content = $file->slurp();
# # warn ref($content);


# my $json = read_file('test2.json');
# # warn ref($json);
# # warn Dumper $json;

