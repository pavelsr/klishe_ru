(function(){
    
    var url = "http://127.0.0.1:3000";


    var app = angular.module('KlisheApp', [ "isteven-multi-select" ]);
    
    app.controller('ServicesCtrl', function($scope, $http, $log) {
        $scope.selectedService = false;
        $http.get(url+'/all_services').success(function(response) {$scope.services = response});

        $scope.countries = [ {name: "Россия", code: "ru" }, {name: "Украина", code: "ua" } ];
        
        $scope.changecompanies = function(){
        if($scope.selectedService){
            $http.get(url+'/service/'+$scope.selectedService.id+'/'+$scope.selectedCountry.code)
             .success(function(data){
                     $scope.companies = data;
                     $scope.selectedCompany = null;                                
                                    })
             .error(function(error){$scope.companies = [];});
            }
            else{
                return [];
            }
        };

        $scope.update_captcha = function(){
            $http.get(url+'/captcha/').success(function(data){$scope.captcha = data.captcha_img; });
        };

/*
         $scope.submit = function(){
            $scope.update_request = {};
            $scope.update_request.values = []
            $scope.update_request.id = $scope.selectedService.id;
            for (var i = 0; i < $scope.selectedCompanies.length; i++) {
             $scope.update_request.values.push($scope.selectedCompanies[i].id); 
            }
         };*/


    });


    app.controller('AddByServiceCtrl', function($scope, $http, $log) {
        $http.get(url+'/all_services').success(function(response) {$scope.services = response});
        $http.get(url+'/all_companies').success(function(response) {$scope.companies = response});
        
        $scope.update = function(){
            $scope.update_request = {};
            $scope.update_request.values = [];
            $scope.update_request.id = $scope.selectedService.id;
             $scope.update_request.type = "by_service";
            for (var i = 0; i < $scope.selectedCompanies.length; i++) {
             $scope.update_request.values.push($scope.selectedCompanies[i].id); 
            }
           // $http.post(url+'/add', { msg: $scope.update_request }).success(function(response) { $scope.server = response });  
           // $http({method: 'POST', url: url+'/add', headers: {'Content-Type': 'application/x-www-form-urlencoded'}, data: { msg: $scope.update_request } }).success(function(response) { $scope.server = response });
           $http({method: 'POST', url: url+'/add', headers: {'Content-Type': 'application/json'}, data: $scope.update_request }).success(function(response) { $scope.server = response });
        };
    });


    app.controller('ShowReport', function($scope, $http, $log) {
        $http.get(url+'/report/service').success(function(response) {$scope.report = response});
    });





})(); 