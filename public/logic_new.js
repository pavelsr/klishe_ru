var api_url = "http://127.0.0.1:3000";

var app = angular.module('KlisheApp', [ 'isteven-multi-select', 'ui.bootstrap' ]);

app.directive('klisheType', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/klishe_type_selector.html',
		controller:function($scope, $http, $log) {
			$scope.klishe = {};
			$scope.klishe.selected_type = false;
			$http.get(api_url+'/all_services').success(function(response) { $scope.klishe.types = response });
			$scope.change_klishe_type = function(){
				$http.get(api_url+'/service/'+$scope.klishe.selected_type.id).success(function(response) {$scope.klishe.available_companies = response});
			}
		}
	}
});

app.directive('orderFields', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/form.html',
	}
});

app.directive('countrySelect', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/country_selector.html',
		controller:function($scope, $http, $log, $filter) {
			$http.get(api_url+'/all_countries').success(function(response) { $scope.klishe.countries = response.available_countries });
			$http.get(api_url+'/all_country_codes').success(function(response) { $scope.klishe.country_codes = response });
			$scope.change_country = function(){
				$scope.klishe.selected_country_plus_code = $filter('filter')($scope.klishe.country_codes, {name: $scope.klishe.selected_country});
				$http.get(api_url+'/service/'+$scope.klishe.selected_type.id+'/'+$scope.klishe.selected_country_plus_code[0].iso).success(function(response) {$scope.klishe.available_companies = response});
			};
		}
	}
});

app.directive('companiesSelect', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/companies_selector.html',
		controller:function($scope, $http, $log) {
			$scope.klishe.selected_companies = [];
			$http.get(api_url+'/all_companies').success(function(response) {$scope.klishe.available_companies = response});
		}
	}
});

app.directive('yaCaptcha', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/captcha.html',
		controller:function($scope, $http, $log) {

			$scope.captcha_update = function(){
				$http.get(api_url+'/captcha/get').success(function(response) {
					$scope.klishe.captcha = response;
					$scope.klishe.captcha.not_checked_or_false = true;
				});
			}
			$scope.captcha_check= function(){
				$http.get(api_url+'/captcha/check', { params: { id: $scope.klishe.captcha.id, user_value: $scope.klishe.captcha.user_value } } ).success(function(response) {
					$scope.klishe.captcha.result = response.result;
					if ($scope.klishe.captcha.result) {
						$scope.klishe.captcha.not_checked_or_false = false;
					};
				});
			}
		}
	}
});

app.directive('submitBtn', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/submit_button.html',
		controller:function($scope, $http, $log, $modal) { 
			$scope.submit_pressed = function (size) {
				var modalInstance = $modal.open({
			      animation: true,
			      templateUrl: 'tpl/modal.html',
			      size: size,		     
			    });
			}
/*			$scope.submit = function(){
            $scope.to = {};
            $scope.to.values = [];
            for (var i = 0; i < $scope.selectedCompanies.length; i++) {
             $scope.to.values.push({email: $scope.selectedCompanies[i].email, name: $scope.selectedCompanies[i].name});
             $http.post('/mail', scope.to.values).then(successCallback); 
            }
         };*/
		}
	}
});


app.directive('klisheDebug', function() {
	return {
		restrict: 'E',
		templateUrl: 'tpl/debug.html',
	}
});


var codes = [
{country:"Russia", code:"ru"},
{country:"Belarus", code:"by"},
{country:"Ukraine", code:"by"}
]; 

var arr = ["Russia", "Belarus"];

merge(codes, arr);

function merge(x,y) {
	console.log(x);
	console.log(y);
}