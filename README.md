# Развёртывание базы данных

Всё, что нужно сделать - запустить скрипт deploy.sh из корня, он всё сделает автоматически.
Время работы скрипта - около 5 минут. Требуется доступ в интернет без особых задержек,
в противном случае работа скрипта может оборваться на этапе парсинга компаний.

Estimated execution time:

real	5m14.737s
user	0m15.255s
sys	0m4.598s

Для работы потребуются установленные Perl-библиотеки. 
Все они перечислены в cpanfile. Установить можно с помощью carton install.

# Что делает скрипт развёртывания?

1. Создаёт базу данных из трёх табличек, структура описаны в create_db.pl
2. Добавляет 12 предустановленных услуг из списка в add_services.pl
3. Скачивает JSON c помощью wget из WP-API, категория "производители". Wget используется из-за того, что
данные не удаётся получить с помощью Mojo::UserAgent из-за невыясненных причин, скорее всего больших таймаутов и объёма данных.
4. Извлекает название компании, город и e-mail при помощи регулярных выражений.
5. Ставит в соответствие городу страну, используя API Яндекс.Карт
6. Заносит это всё в базу данных SQLite
7. Случайным образом заменяет реальные e-mail адреса на 5 тестовых (прописаны в add_test_emails.pl)
8. Случайным образом ставит в соответствие каждой компании от 1 до 12 услуг, установленных в п.2

# Фронтенд

Current files:

index_new.html
logic_new.js

https://github.com/isteven/angular-multi-select

Глобальный объект - $scope.klishe
В него вложено всё остальное

# Бэкенд

## Зависимости

### Utilities

curl

### Perl

# Методы API 

## SMTP API





