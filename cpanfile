requires 'Data::Dumper';
requires 'Mojolicious::Lite';
requires 'DBI';
requires 'DBD::SQLite';
requires 'Email::Simple';
requires 'Email::Sender::Simple';
requires 'Email::Sender::Transport::SMTPS';
requires 'common::sense';
