#!/usr/bin/env perl
use DBI;
use feature 'say';

my $dbh = DBI->connect("dbi:SQLite:dbname=klishe.db","","");
create_table($dbh);

sub create_table {
	my $dbh = shift;
	my $sql = <<'END_SQL';
CREATE TABLE companies (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created DEFAULT CURRENT_TIMESTAMP,
    name VARCHAR(160),
    email VARCHAR(89),
    location VARCHAR(50),
    city VARCHAR(50),
    country VARCHAR(50),
    url VARCHAR(30)
    )
END_SQL
	$dbh->do($sql);	

    $sql = <<'END_SQL';
CREATE TABLE services (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created DEFAULT CURRENT_TIMESTAMP,
    name VARCHAR(160),
    has_production INTEGER
    )
END_SQL
    $dbh->do($sql); 

    $sql = <<'END_SQL';
CREATE TABLE possibilities (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    company_id INTEGER,
    service_id INTEGER,
    quality INTEGER
    )
END_SQL
    $dbh->do($sql); 
	
	return 0;
}
