#!/usr/bin/env perl

use common::sense;
use Mojo::UserAgent;
use Mojo::JSON;
use Data::Dumper;
use DBI;
use feature 'say';

my $dbh = DBI->connect("dbi:SQLite:dbname=klishe.db","","");
$dbh->{sqlite_unicode} = 1;
my $table = "companies";
my $file = 'companies.json';

my $content = slurp($file);
my $companies = Mojo::JSON::decode_json $content;  # decode_json , from_json or j
warn Dumper scalar @$companies;


# test_email_regexp($companies, 7);
# say get_country_by_city('Ростов-на-Дону');

my $array = parse_companies($companies);   # return array of hashes

say "End of parsing. Saving from RAM to database";
for (@$array) {
	insert_in_db($_);
}

# warn Dumper $array;

sub slurp {
    my $file = shift;
    open my $fh, '<', $file or die;
    local $/ = undef;
    my $cont = <$fh>;
    close $fh;
    return $cont;
}

sub test_email_regexp {
	# Function just to test emails
	my ($companies, $n) = @_;
	my $test_email = $companies->[$n]->{content};
	if ($test_email =~ /(\w+@[\w.-]+|\{(?:\w+, *)+\w+\}@[\w.-]+)/) {
		say "ok";
		say $1;
	} else {
		say "email not match regexp";
		say "please check ".$companies->[$n]->{guid};
	}
	return 0;
};

sub get_country_by_city {
	my $city = shift;
	my $ua = Mojo::UserAgent->new;
	# http://geocode-maps.yandex.ru/1.x/?geocode=Ростов&format=json
	#warn Dumper $ua->get('https://geocode-maps.yandex.ru/1.x/?geocode=$city&format=json')->res->json;
	my $country = $ua->get('https://geocode-maps.yandex.ru/1.x/?geocode='.$city.'&format=json')->res->json->{response}->{GeoObjectCollection}->{featureMember}->[0]->{GeoObject}->{metaDataProperty}->{GeocoderMetaData}->{AddressDetails}->{Country}->{CountryName};
	#warn Dumper $country;
	return $country;
}

sub parse_companies {
	my $companies = shift;
	my $i = 1;
	my @a;
	for (@$companies) {
		my @comp_city = split(',', $_->{title});
		my $company = @comp_city[0];
		my $city = @comp_city[1];
		my $email_string = $_->{content};
		my $email;
		if ($email_string =~ /(\w+@[\w.-]+|\{(?:\w+, *)+\w+\}@[\w.-]+)/) {	
			$email = $1;
		} else {
			$email = 0;
		};		
		$company =~s/['"«»&#;.]+//g;
		my $hash = {};
		$hash->{url} = $_->{guid};
		$hash->{name}=$company;
		$hash->{city}=$city;
		$hash->{country}=get_country_by_city($city);
		$hash->{email} = $email;
		say $i." | ".$hash->{name}." | ".$hash->{city}." | ".$hash->{country}." | ".$hash->{email}." | ".$hash->{url};
		$i++;

		# say $company." | ".$city." | ".$country;
		# say $company." | ".$city." | ".$country." | ".$email;
		push @a, $hash;
	}
	return \@a;
};

sub insert_in_db {
	my $hash = shift;
	my $h = prepare_sql($hash);
	$dbh->do("INSERT INTO ".$table." (".$h->{'fields'}.") VALUES (".$h->{'values'}.")");
	return 0;
}

sub prepare_sql {
	my $hash = shift;
	my @fields;
	my @values;
	foreach my $key ( keys %$hash ) {
		push @fields, $key;
		push @values, "'".$hash->{$key}."'";
	}
	my $new_hash;
	$new_hash->{'fields'} = join(", ", @fields);
	$new_hash->{'values'} = join(", ", @values);
	return $new_hash;
}