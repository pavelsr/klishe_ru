#!/usr/bin/env perl

use common::sense;
use Mojo::JSON;
use Data::Dumper;
use DBI;
use feature 'say';

my $dbh = DBI->connect("dbi:SQLite:dbname=klishe.db","","");
$dbh->{sqlite_unicode} = 1;
my $table = "companies";

sub random_email {
	# my @emails = ('gra321@yandex.ru', 
	# 	'gravirovka@mail.ru', 
	# 	'klisheru@gmail.com', 
	# 	'medali@mail.ru', 
	# 	'medali@yandex.ru');
	my @emails = ('pavel@fablab61.ru', 'pavel.p.serikov@gmail.com');
	my $random_element = $emails[rand @emails];
	return $random_element;
}

update_column();

sub update_column {
	my $hash_ref = $dbh->selectall_hashref("SELECT id, email FROM $table ORDER BY id", 'id');
	warn Dumper $hash_ref;
	for my $key ( sort {$a<=>$b} keys %$hash_ref ) {
		say "ID $key: ".$hash_ref->{$key}->{email}. " -> ".random_email();
		my $email = random_email();
		my $query = "UPDATE $table SET email = ? WHERE id = ?";
		my $res = $dbh->prepare($query) or die("cannot prepeare");
		$res->execute($email, $key);

		# there is some unknown bug in do() method so I used prepare - execute
		# $dbh->do(q{UPDATE companies SET email = ? WHERE id = ?}, 'id', $email, $key) or die $dbh->errstr;
		# $dbh->commit;
	}

}


