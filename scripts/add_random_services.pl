#!/usr/bin/env perl

### Rules
# На одну компанию (company_id) приходится минимум одна услуга (service_id). Можно изменить второй параметр в функции random_ids_array
# Максимальное количество услуг = третий параметр в функции random_ids_array()
# Услуги выбираются случайно на основе их id в базе данных (service id)

use common::sense;
use Mojo::JSON;
use Data::Dumper;
use DBI;
use feature 'say';

my $table = "companies";
my $dbh = DBI->connect("dbi:SQLite:dbname=klishe.db","","");
$dbh->{sqlite_unicode} = 1;

my $max_service_id = $dbh->selectrow_arrayref("SELECT id FROM services ORDER BY id DESC")->[0];
# my $id = $dbh->last_insert_id("", "", "companies", ""); # doens't work somehow

my $test_array = random_ids_array($max_service_id, 1, $max_service_id);   # random_ids_array(max_id, $min_size_of_array, $min_size_of_array)
print join(", ", @$test_array)."\n";


my $max_company_id = $dbh->selectrow_arrayref("SELECT id FROM companies ORDER BY id DESC")->[0];

my $c;
for my $i (1..$max_company_id) {
	my $a = random_ids_array($max_service_id, 1, $max_service_id);
	for my $j (0..scalar @$a-1) {
		my $k = random_quality();
		my $t = $a->[$j];
		say "INSERT INTO possibilities(company_id, service_id, quality) VALUES ($i, $t, $k)";
		$dbh->do("INSERT INTO possibilities(company_id, service_id, quality) VALUES ($i, $j, 1)");
		$c++;
	}
    # print "$i\n";
}
say "Total inserted possibilities count: ".$c;

sub random_quality {
	my @q = (1..5);
	my $rand = $q[rand @q];
	return $rand;
}

sub random_size_or_id {
	my ($min_num, $max_num) = @_;
	my @n = ($min_num..$max_num);
	my $rand = $n[rand @n];
	return $rand;
}

sub random_ids_array {
	my ($max_id, $min_size, $max_size) = @_;
	my @sizes = ($min_size..$max_size);		# possible sizes
	my $size = $sizes[rand @sizes];
	say "Size of random array: ".$size;
	my @array;

	my $i=0;
	while ($i < $size) {
		my $new_elem = random_size_or_id(1, $max_id); # get random id
		# say $new_elem;   # just for debuging of appeared elements
		if ( ! exists_1($new_elem, \@array) ) {
				push @array, $new_elem;
				$i++;
			}
		}
	return \@array;
} 


# my @b = (1,2,3,4,5);
# say exists_1(3, $b);

sub exists_1 {
	# Check that element exists in arraycl
	my ($el, $array) = @_;
	if (grep( /^$el$/, @$array ) ) {
		return 1;
	} else {
		return 0;
	}
}



# sub update_column {
# 	my $hash_ref = $dbh->selectall_hashref("SELECT id, email FROM $table ORDER BY id", 'id');
# 	for my $key ( sort {$a<=>$b} keys %$hash_ref ) {
# 		say "ID $key: ".$hash_ref->{$key}->{email}. " -> ".random_email();
# 		my $email = random_email();
# 		my $query = "UPDATE $table SET email = ? WHERE id = ?";
# 		my $res = $dbh->prepare($query) or die("cannot prepeare");
# 		$res->execute($email, $key);

# 		# there is some unknown bug in do() method so I used prepare - execute
# 		# $dbh->do(q{UPDATE companies SET email = ? WHERE id = ?}, 'id', $email, $key) or die $dbh->errstr;
# 		# $dbh->commit;
# 	}

# }


