#!/usr/bin/env perl
use perl5lib;

use DBI;
use feature 'say';
use common::sense;

my $dbh = DBI->connect("dbi:SQLite:dbname=klishe.db","","");
$dbh->{sqlite_unicode} = 1;

my @AoH = (
	    {
	       name  => "Россия",
	       iso     => "ru",
	    },
	    {
	       name  => "Украина",
	       iso     => "ua",
	    },
	    {
	       name  => "Казахстан",
	       iso     => "kz",
	    },
	        {
	       name  => "Беларусь",
	       iso     => "be",
	    },
	  );

create_table($dbh);
add_samples($dbh, \@AoH);


sub create_table {
	my $dbh = shift;
	my $sql = <<'END_SQL';
CREATE TABLE country_codes (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(160),
    iso VARCHAR(2)
    )
END_SQL
	$dbh->do($sql);	
}


sub add_samples {
	my ($dbh, $a) = @_;
	# in future I'm plan to use https://metacpan.org/pod/Country::Codes for this purpose
	my $sth;
	
	 for (@$a) {
	        $sth = $dbh->prepare("INSERT INTO country_codes(name, iso) VALUES (? , ?)");
	        $sth->execute( $_->{name}, $_->{iso} );
	    }

}

# push @AoH, { name => "Англия", iso => "en" };
